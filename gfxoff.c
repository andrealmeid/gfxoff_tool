/*
 * Tool to interact with gfxoff debugfs. It assumes amdgpu is dri/0
 *
 * Copyright Igalia
 * Author André Almeida <andrealmeid@igalia.com>
 *
 * NOTE: the userspace is responsible for taking care of gfxoff request count
 * (gfx.gfx_off_req_count), so it's really advised to have only one process/thread
 * writing at amdgpu_gfxoff file. One should always make sure it's disabled before
 * enabling it, and vice-versa, otherwise it's easy to get lost in the math.
 *
 * License: GPLv2
 */

#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

#define DRV_PATH "/sys/kernel/debug/dri/0/"

#define ENABLE_TIMEOUT 90

void show_help()
{
	printf("gfxoff helper tool\n\t--help, -h: show this\n\n");
	printf("\t--status, -s: show current status (in/out GFXOFF status)\n");
	printf("\t--state, -e: show state (allowed/disallowed)\n");
	printf("\t--count, -c: show entrycount\n");
	printf("\t--residency, -r: show last residency\n");
	printf("\t--all, -a: show all stats\n\n");
	printf("\t--enable: enable gfxoff\n");
	printf("\t--disable: disable gfxoff\n");
	printf("\t--residency_on: start residency logging\n");
	printf("\t--residency_off: stop residency logging\n");
}

int get_state(bool quiet)
{
	uint32_t state;
	int ret, fd = open(DRV_PATH "amdgpu_gfxoff", O_RDONLY);

	if (fd == -1) {
		printf("open error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	ret = read(fd, &state, sizeof(state));
	if (ret == -1) {
		printf("read error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	close(fd);

	if (quiet)
		return state;

	if (state)
		printf("GFXOFF enabled\n");
	else
		printf("GFXOFF disabled\n");

	return state;
}

// get current status of GFXOFF and print in a nice string
int get_status(bool quiet)
{
	uint32_t status;
	int ret, fd = open(DRV_PATH "amdgpu_gfxoff_status", O_RDONLY);

	if (fd == -1) {
		printf("open error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	ret = read(fd, &status, sizeof(status));
	if (ret == -1) {
		printf("read error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	close(fd);

	if (quiet)
		return status; 

	printf("Status: %d ", status);

	switch (status) {
	case 0:
		printf("In GFXOFF state\n");
		break;
	case 1:
		printf("Transition out of GFXOFF State\n");
		break;
	case 2:
		printf("Not in GFXOFF State\n");
		break;
	case 3:
		printf("Transition into GFXOFF State\n");
		break;
	default:
		printf("Unknown state\n");
	}

	return status;
}

// get total gfxoff entry count at the time of query since system power-up
int get_count(bool quiet)
{
	int ret, fd = open(DRV_PATH "amdgpu_gfxoff_count", O_RDONLY);
	uint32_t value;

	if (fd == -1) {
		printf("open error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	ret = read(fd, &value, sizeof(value));
	if (ret == -1) {
		printf("read error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	if (!quiet)
		printf("Entrycount: %d\n", value);

	return value;
}

// enable/disable residency logging
int set_residency(bool enable)
{
	int ret, fd = open(DRV_PATH "amdgpu_gfxoff_residency", O_RDWR);
	uint32_t value;

	if (fd == -1) {
		printf("open error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	value = enable ? 0xff : 0x00;

	ret = write(fd, &value, sizeof(value));
	if (ret == -1) {
		printf("write error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	return value;
}

// Average gfxoff residency % during the logging interval
int get_residency(bool quiet)
{
	int ret, fd = open(DRV_PATH "amdgpu_gfxoff_residency", O_RDONLY);
	uint32_t value;
	float residency;

	if (fd == -1) {
		printf("open error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	ret = read(fd, &value, sizeof(value));
	if (ret == -1) {
		printf("read error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}

	residency = value / 100.0;

	if (!quiet)
		printf("Last logged residency: %.2f%%\n", residency);

	return value;
}
  
// sync enable/disable gfxoff status, while taking care to not mess anything
int set_status(bool enable)
{
	int state = get_state(true), tries = 0, current_state, fd;
	uint32_t value;

	fd = open(DRV_PATH "amdgpu_gfxoff", O_RDWR);

	if (fd == -1) {
		printf("open error: (%d) %s\n", errno, strerror(errno));
		return errno;
	}
	
	if (enable && state) {
		printf("Already enabled, nothing to do\n");
	} else if (!enable && !state) {
		printf("Already disabled, nothing to do\n");
	} else if (enable) {
		value = 0xff;
		write(fd, &value, sizeof(value));
		printf("GFXOFF enabled, waiting for it to start working... \n");
		
		current_state = get_state(true);
		while (tries < ENABLE_TIMEOUT && current_state == 0) {
			current_state = get_state(true);
			sleep(1);
			tries++;
		}

		if (current_state)
			printf("Enabled in %d secs\n", tries); 
		else
			printf("Enable timeout (imbalance bug?)\n");
	} else {
		value = 0x00;
		write(fd, &value, sizeof(value));
		printf("GFXOFF disabled\n");
	}

	close(fd);

	return 0;
}

int main(int argc, char **argv)
{
	bool show_count = false, show_residency = false, show_status = false;
	bool show_state = false;
	int i;

	if (argc == 1)
		show_help();

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
			show_help();
			return 0;
		}

		if (!strcmp(argv[i], "--status") || !strcmp(argv[i], "-s"))
			show_status = true;

		if (!strcmp(argv[i], "--state") || !strcmp(argv[i], "-e"))
			show_state = true;

		if (!strcmp(argv[i], "--count") || !strcmp(argv[i], "-c"))
			show_count = true;

		if (!strcmp(argv[i], "--residency") || !strcmp(argv[i], "-r"))
			show_residency = true;

		if (!strcmp(argv[i], "--all") || !strcmp(argv[i], "-a"))
			show_count = show_status = show_residency = show_state = true;

		if (!strcmp(argv[i], "--enable"))
			set_status(true);

		if (!strcmp(argv[i], "--disable"))
			set_status(false);

		if (!strcmp(argv[i], "--residency_on"))
			set_residency(true);

		if (!strcmp(argv[i], "--residency_off"))
			set_residency(false);
	}

	if (show_state)
		get_state(false);

	if (show_status)
		get_status(false);

	if (show_count)
		get_count(false);

	if (show_residency)
		get_residency(false);

	return 0;
}
