# gfxoff_tool

A tool to interact with AMD's GFXOFF

---

From [Linux kernel](https://docs.kernel.org/gpu/amdgpu/thermal.html#gfxoff) docs:

> GFXOFF is a feature found in most recent GPUs that saves power at runtime. The
> card's RLC (RunList Controller) firmware powers off the gfx engine
> dynamically when there is no workload on gfx or compute pipes. GFXOFF is on by
> default on supported GPUs.

Using the tool:

```
	--help, -h: show this

	--status, -s: show current status (in/out GFXOFF status)
	--state, -e: show state (allowed/disallowed)
	--count, -c: show entrycount
	--residency, -r: show last residency
	--all, -a: show all stats

	--enable: enable gfxoff
	--disable: disable gfxoff
	--residency_on: start residency logging
	--residency_off: stop residency logging
```

Use with `watch` to have a live report:

```
$ sudo watch -n0.5 ./gfxoff --all

Every 0.5s: ./gfxoff --all		    steamdeck: Fri Jul 22 17:06:01 2022

GFXOFF enabled
Status: 0 In GFXOFF state
Entrycount: 3469
Last logged residency: 32.84%
```
